// import axios from "axios";
const url = 'https://pokeapi.co/api/v2/';
export async function getPokemons(cant) {
  try {
    let response = await fetch(url + `pokemon/?limit=${cant}&offset=5`)
      .then(res => res.json())
      .catch(err => console.error(err));
    for await (const pokemon of response.results) {
      pokemon.data = await getDetailsPokemons(pokemon.url);
      pokemon.img = pokemon.data.sprites.front_default;
    }
    return response;
  } catch (error) {
    return [];
  }
}
export async function getPokemonsPaginate(url) {
  try {
    let response = await fetch(url)
      .then(res => res.json())
      .catch(err => console.error(err));
    for await (const pokemon of response.results) {
      pokemon.data = await getDetailsPokemons(pokemon.url);
      pokemon.img = pokemon.data.sprites.front_default;
    }
    return response;
  } catch (error) {
    return [];
  }
}
export async function getPokeDetails(search) {
  let data = await fetch(`https://pokeapi.co/api/v2/pokemon/${search}`)
    .then(res => res.json())
    .catch(err => console.error(err));
  if (data) {
    let pokemonData = {
      img: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${search}.png`,
      imgJuego: data.sprites.front_default,
      imgCvg: data.sprites.other.dream_world.front_default,
      name: data.name,
      experiencia: data.base_experience,
      hp: data.stats[0].base_stat,
      ataque: data.stats[1].base_stat,
      defensa: data.stats[2].base_stat,
      especial: data.stats[3].base_stat
    };
    return pokemonData;
  }
  return false;
}
async function getDetailsPokemons(url) {
  return await fetch(url)
    .then(async res => await res.json())
    .catch(() => []);
}
